
## T4: Tic-Tac-Toe-TCP

Multiplayer Tic-Tac-Toe game using network communication via TCP sockets and threads.
Uses the old fixed function OpenGL and GLUT to render a simple GUI. Class assignment.

----

![Tic-Tac-Toe-TCP](https://bytebucket.org/glampert/tic-tac-toe-tcp/raw/79a813380bb8d84bc266564945efc367e5697c58/T4.jpg "T4: Tic-Tac-Toe-TCP")

----

This project's source code is released under the [MIT License](http://opensource.org/licenses/MIT).

