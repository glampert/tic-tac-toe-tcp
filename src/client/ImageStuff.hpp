
#ifndef T4_IMAGE_STUFF_HPP
#define T4_IMAGE_STUFF_HPP

#ifdef _WIN32
#include <Windows.h>
#include <GL/glut.h>
#elif defined(__APPLE__)
#include <GLUT/glut.h>
#else
#include <GL/glut.h>
#endif

#include <stdlib.h>
#include <deque>

// =========================================================
// Image Utilities
// =========================================================

// Static non-animated image
struct static_image_t
{
    unsigned int width;
    unsigned int height;
    unsigned int channels;
    unsigned int buffsiz;
    void * pixels;
};

// Loads 24bit and 32bit BMP images.
static_image_t * LoadImageFromFile(const char * fileName);

// Free the allocated memory.
void FreeImage(static_image_t ** ppImage);

// Creates a scaled version of the image passed in.
static_image_t * ScaleImage(const static_image_t * pImageIn, int newWidth, int newHeight);

// This function expects the proper GL states to be set, such as an ortho 2D projection, color enabled, etc...
void DrawImage(int x, int y, const static_image_t * pImage);

// =========================================================
// Sprite2D Class
// =========================================================

class Sprite2D
{
  public:

    Sprite2D(bool free_images_on_death = true);
    ~Sprite2D();

    void PushFrame(const static_image_t * pImage, float delay);

    void Draw(int x, int y, unsigned int firstFrame, unsigned int lastFrame);
    void Draw(int x, int y);

    static void UpdateAnimationTimer();

    const static_image_t * GetFrame(unsigned int frame) const;

    unsigned int CurrentFrame() const;
    unsigned int NumFrames() const;

    void SetPerFrameDelay(float delay);

    void FreeImagePointers();

  protected:

    struct frame_t
    {
      public:

        frame_t()
            : image_pointer(0), delay(0.0f)
        {
        }

        frame_t(const frame_t & r)
            : image_pointer(r.image_pointer), delay(r.delay)
        {
        }

        frame_t(const static_image_t * pImage, float delay)
            : image_pointer(pImage), delay(delay)
        {
        }

        frame_t & operator=(const frame_t & r)
        {
            image_pointer = r.image_pointer;
            delay = r.delay;
            return *this;
        }

        const static_image_t * image_pointer;
        float delay;
    };

  private:

    static float m_elapsed_time;
    static double m_time[2];

    typedef std::deque<frame_t> frame_deque;
    frame_deque m_image_frames;

    const bool kill_ptrs;
    unsigned int m_current_frame;
    float m_time_count;
};

// =========================================================
// glPrintf (For Simple Text Drawing)
// =========================================================

// Works like the regular 'printf' C function.
int glPrintf(int x, int y, const char * format, ...);

// =========================================================
// Button2D Class
// =========================================================

class Button2D
{
  public:

    Button2D();
    Button2D(static_image_t * _image, int _x, int _y);
    ~Button2D();

    void SetImage(static_image_t * _image, int _x, int _y);
    static_image_t * returnImage() const;

    void FreeMemory();
    void DrawButton() const;
    bool testColision(int _x, int _y) const;

    int getX() const { return x; };
    int getY() const { return y; };

  private:

    static_image_t * image;
    int x, y;
};

#endif // T4_IMAGE_STUFF_HPP
