
#ifndef T4_CLIENT_MAIN_HPP
#define T4_CLIENT_MAIN_HPP

#include "Common.hpp"

// =========================================================
// [T4] - Tic Tac Toe TCP - Client
// =========================================================

// Game related initialization.
bool InitGame();

// Free game resources.
void EndGame();

// Try to connect to the server.
bool Connect();

// Close network connection.
void CloseConnection();

// OpenGL initialization code.
void InitOpenGL();

// Free all resources and kill the application. Only called by the 'atexit()' function.
void Terminate();

// Render one frame to the game window. Called by 'glutMainLoop()'.
void DisplayCallback();

// Compute the points for the score line over the winner pieces.
void MakeScoreLine(struct Line & line);

// Reshape the window. Blocks user reshape.
void ReshapeWindow(int w, int h);

// Save the mouse x and y pos. Only called by GLUT.
void MouseMotionCallback(int x, int y);

// Checks mouse button click. Only called by GLUT.
void MouseCallback(int button, int state, int x, int y);

// Update keyboard. Only called by GLUT.
void KeyboardCallback(unsigned char key, int x, int y);

// The only purpose of this thread is to update the game data in real time,
// avoiding the screen from freeze if the server takes too long to reply.
class ComunicationsThread
{
  public:

    ComunicationsThread();
    ~ComunicationsThread();

    static void threadProc(void * param);
    void begin();

  private:

    // Private mutex lock.
    Mutex _mtx;

    // Signals to the player thread to terminate.
    char _terminate_thread_flag;

    Thread * _t;
};

#endif // T4_CLIENT_MAIN_HPP
