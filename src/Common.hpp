
#ifndef T4_COMMON_HPP
#define T4_COMMON_HPP

// =========================================================
// Common Includes And Definition For The Application
// =========================================================

#include <iostream>
#include <fstream>
#include <string>
#include <vector>
#include <stdlib.h>
#include <assert.h>

#include "Socket.hpp"
#include "Thread.hpp"

#ifdef _MSC_VER
// Disable The CRT Lib Deprecation Warning on VS.
#pragma warning(disable : 4996)
#endif // _MSC_VER

#define LOG_MSG(x)              \
    do                          \
    {                           \
        std::cout << x << '\n'; \
    } while (0)

#define LOG_ERROR(x)                         \
    do                                       \
    {                                        \
        std::cout << "Error: " << x << '\n'; \
    } while (0)

#define LOG_WARNING(x)                         \
    do                                         \
    {                                          \
        std::cout << "Warning: " << x << '\n'; \
    } while (0)

#define LOG_FATAL_ERROR(x)                                                               \
    do                                                                                   \
    {                                                                                    \
        std::cout << "Fatal Error! " << x << ". Terminating the application..." << '\n'; \
        exit(EXIT_FAILURE);                                                              \
    } while (0)

// Unix<=>Win portability macros:
#ifdef _WIN32
#define CLEAR_CONSOLE() system("cls")
#define SLEEP_MS(millisec) Sleep(millisec)
#else
#define SLEEP_MS(millisec) usleep((millisec)*1000)
#define CLEAR_CONSOLE() system("clear")
#endif

//
// When this directive is set the compiler will enable sleep() calls
// in the main threads, which may be good on less powerful machines, since this
// will decrease the processor load. Note that this is a very rudimentary way of
// controlling CPU usage, but will do for now....
//#define DROP_FRAME_RATE
//
#define COOL_DOWN_TIME 5

// == Game Related Data ======

// The Tic Tac Toe board sizes:
#define BOARD_ROWS 3
#define BOARD_COLS 3

// Player state flags:
#define PLAYER_WAITING (1 << 0)
#define PLAYER_IN_GAME (1 << 1)
#define PLAYER_WON     (1 << 2)
#define PLAYER_LOST    (1 << 3)
#define PLAYER_DRAW    (1 << 4)

struct ServerMessage
{
    int playerFlags;                        // Player status (Win, Lose, etc...)
    char gameBoard[BOARD_ROWS][BOARD_COLS]; // Updated board.
};

struct ClientMessage
{
    // What the player changed.
    int updatedRow;
    int updatedCol;
};

#define ASCII_MSG_MAX_LEN 64

#endif // T4_COMMON_HPP
