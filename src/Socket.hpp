/*
   Socket.hpp

   Copyright (C) 2002-2004 Ren� Nyffenegger

   Modified By Guilherme R. Lampert On November - 2010
   #Lampert: September 2012 - Made fully compatible with Linux/MacOS

   This source code is provided 'as-is', without any express or implied
   warranty. In no event will the author be held liable for any damages
   arising from the use of this software.

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this source code must not be misrepresented; you must not
      claim that you wrote the original source code. If you use this source code
      in a product, an acknowledgment in the product documentation would be
      appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
      misrepresented as being the original source code.

   3. This notice may not be removed or altered from any source distribution.

   Ren� Nyffenegger rene.nyffenegger@adp-gmbh.ch
*/

// SRC:
// http://www.adp-gmbh.ch/win/misc/sockets.html

#ifndef T4_SOCKET_HPP
#define T4_SOCKET_HPP

#ifdef _WIN32
#include <WinSock2.h>
#else
#include <unistd.h>
#include <fcntl.h>
#include <sys/types.h>
#include <sys/socket.h>
#include <netinet/in.h>
#include <arpa/inet.h>
#include <netdb.h>
#include <errno.h>
typedef int SOCKET;
#ifndef INVALID_SOCKET
#define INVALID_SOCKET (-1)
#define SOCKET_ERROR   (-1)
#endif
#endif

#include <string>

enum TypeSocket
{
    BlockingSocket,
    NonBlockingSocket
};

class Socket
{
  public:

    virtual ~Socket();
    Socket(const Socket &);
    Socket & operator=(Socket &);

    void Close();

    // These two functions now receive raw data buffers as arguments.
    // RecieveLine() and SendLine() are removed for good because those functions were unused.
    bool ReceiveBytes(char * buff, size_t sizeInBytes) const;
    bool SendBytes(const char * buff, size_t sizeInBytes) const;

  protected:

    friend class SocketServer;
    friend class SocketSelect;

    Socket(SOCKET s);
    Socket();

    SOCKET s_;
    int * refCounter_;

  private:

    static void Start();
    static void End();
    static int nofSockets_;
};

class SocketClient : public Socket
{
  public:

    SocketClient(const std::string & host, int port);
};

class SocketServer : public Socket
{
  public:

    SocketServer(int port, int connections, TypeSocket type = BlockingSocket);
    Socket * Accept();
};

// http://msdn.microsoft.com/library/default.asp?url=/library/en-us/winsock/wsapiref_2tiq.asp

class SocketSelect
{
  public:

    SocketSelect(Socket const * const s1, Socket const * const s2 = NULL, TypeSocket type = BlockingSocket);
    bool Readable(Socket const * const s);

  private:

    fd_set fds_;
};

#endif // T4_SOCKET_HPP
