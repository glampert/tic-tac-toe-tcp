/*
   Socket.cpp

   Copyright (C) 2002-2004 Ren� Nyffenegger

   Modified By Guilherme R. Lampert On November - 2010
   #Lampert: September 2012 - Made fully compatible with Linux/MacOS

   This source code is provided 'as-is', without any express or implied
   warranty. In no event will the author be held liable for any damages
   arising from the use of this software.

   Permission is granted to anyone to use this software for any purpose,
   including commercial applications, and to alter it and redistribute it
   freely, subject to the following restrictions:

   1. The origin of this source code must not be misrepresented; you must not
      claim that you wrote the original source code. If you use this source code
      in a product, an acknowledgment in the product documentation would be
      appreciated but is not required.

   2. Altered source versions must be plainly marked as such, and must not be
      misrepresented as being the original source code.

   3. This notice may not be removed or altered from any source distribution.

   Ren� Nyffenegger rene.nyffenegger@adp-gmbh.ch
*/

#include "Socket.hpp"
#include <iostream>

#ifdef _MSC_VER
#pragma comment(lib, "Ws2_32.lib")
#endif

using namespace std;

int Socket::nofSockets_ = 0;

void Socket::Start()
{

#ifdef _WIN32
    if (!nofSockets_)
    {
        WSADATA info;
        if (WSAStartup(MAKEWORD(2, 0), &info))
        {
            throw "Could not start WSA";
        }
    }
#endif

    ++nofSockets_;
}

void Socket::End()
{

#ifdef _WIN32
    WSACleanup();
#endif
}

Socket::Socket()
    : s_(0)
{
    Start();
    // UDP: use SOCK_DGRAM instead of SOCK_STREAM
    s_ = socket(AF_INET, SOCK_STREAM, 0);

    if (s_ == INVALID_SOCKET)
    {
        throw "INVALID_SOCKET";
    }

    refCounter_ = new int(1);
}

Socket::Socket(SOCKET s)
    : s_(s)
{
    Start();
    refCounter_ = new int(1);
}

Socket::~Socket()
{
    if (!--(*refCounter_))
    {
        Close();
        delete refCounter_;
    }

    --nofSockets_;
    if (!nofSockets_)
        End();
}

Socket::Socket(const Socket & o)
{
    refCounter_ = o.refCounter_;
    (*refCounter_)++;
    s_ = o.s_;

    nofSockets_++;
}

Socket & Socket::operator=(Socket & o)
{
    (*o.refCounter_)++;

    refCounter_ = o.refCounter_;
    s_ = o.s_;

    nofSockets_++;

    return *this;
}

void Socket::Close()
{

#ifdef _WIN32
    closesocket(s_);
#else
    close(s_);
#endif

    s_ = INVALID_SOCKET;
}

bool Socket::ReceiveBytes(char * buff, size_t sizeInBytes) const
{
    return (recv(s_, buff, sizeInBytes, 0) != SOCKET_ERROR);
}

bool Socket::SendBytes(const char * buff, size_t sizeInBytes) const
{
    return (send(s_, buff, sizeInBytes, 0) != SOCKET_ERROR);
}

SocketServer::SocketServer(int port, int connections, TypeSocket type)
{
    sockaddr_in sa;

    memset(&sa, 0, sizeof(sa));

    sa.sin_family = PF_INET;
    sa.sin_port = htons(port);
    errno = 0;
    s_ = socket(AF_INET, SOCK_STREAM, 0);
    if (s_ == INVALID_SOCKET || s_ < 0 || errno != 0)
    {
        throw strerror(errno);
    }

    if (type == NonBlockingSocket)
    {
#ifdef _WIN32
        u_long arg = 1;
        ioctlsocket(s_, FIONBIO, &arg);
#else
        fcntl(s_, F_SETFL, O_NONBLOCK);
#endif
    }

    /* bind the socket to the internet address */
    if (bind(s_, (sockaddr *)&sa, sizeof(sockaddr_in)) != 0)
    {
#ifdef _WIN32
        closesocket(s_);
#else
        close(s_);
#endif
        s_ = INVALID_SOCKET;
        throw strerror(errno);
    }

    if (listen(s_, connections) != 0)
    {
        throw strerror(errno);
    }
}

Socket * SocketServer::Accept()
{

    SOCKET new_sock = accept(s_, NULL, NULL);

    if (new_sock == INVALID_SOCKET)
    {

#ifdef _WIN32
        int rc = WSAGetLastError();
        if (rc == WSAEWOULDBLOCK)
        {
            return 0; // non-blocking call, no request pending
        }
#else
        if (errno == EAGAIN)
        {
            return 0; // non-blocking call, no request pending
        }
#endif
        else
        {
            throw "Invalid Socket";
        }
    }

    Socket * r = new Socket(new_sock);
    return r;
}

SocketClient::SocketClient(const std::string & host, int port)
    : Socket()
{
    std::string error;

    hostent * he;
    if ((he = gethostbyname(host.c_str())) == 0)
    {
        error = strerror(errno);
        throw error;
    }

    sockaddr_in addr;
    addr.sin_family = AF_INET;
    addr.sin_port = htons(port);
    addr.sin_addr = *((in_addr *)he->h_addr);
    memset(&(addr.sin_zero), 0, 8);

    if (::connect(s_, (sockaddr *)&addr, sizeof(sockaddr)))
    {

#ifdef _WIN32
        error = strerror(WSAGetLastError());
#else
        error = strerror(errno);
#endif
        throw error;
    }
}

SocketSelect::SocketSelect(Socket const * const s1, Socket const * const s2, TypeSocket type)
{

#ifndef _MSC_VER
#define TIMEVAL struct timeval
#endif

    FD_ZERO(&fds_);
    FD_SET(const_cast<Socket *>(s1)->s_, &fds_);
    if (s2)
    {
        FD_SET(const_cast<Socket *>(s2)->s_, &fds_);
    }

    TIMEVAL tval;
    tval.tv_sec = 0;
    tval.tv_usec = 1;

    TIMEVAL * ptval;
    if (type == NonBlockingSocket)
    {
        ptval = &tval;
    }
    else
    {
        ptval = 0;
    }

    if (select(0, &fds_, (fd_set *)0, (fd_set *)0, ptval) == SOCKET_ERROR)
        throw "Error in select";
}

bool SocketSelect::Readable(Socket const * const s)
{
    if (FD_ISSET(s->s_, &fds_))
        return true;
    return false;
}
