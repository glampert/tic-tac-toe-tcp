
#ifndef T4_GAME_SERVER_HPP
#define T4_GAME_SERVER_HPP

class ClientThread;

// =========================================================
// Game Server Class (Singleton)
// =========================================================

class GameServer
{
  public:

    // Singleton lifetime management:
    static GameServer * instance();
    static void kill();

    // Start the server and load preferences from a configuration file.
    bool init(const char * cfgFile);

    // Wait for players to connect until max connections is reached.
    void waitForConnections(int maxConnections = 2);

    // Runs the game logic.
    void startGame();

    // Reset the game.
    void resetGame();

    // Actually tests if the player won, lost or draw the game.
    // Returns the new player flags.
    int checkMyGameState(char player);

    // Allow synchronized read only access to the board.
    void readBoard(char buff[BOARD_ROWS][BOARD_COLS]) const;
    void readBoardElement(char & elem, int row, int col) const;

    // Allow synchronized write only access to the board.
    void writeBoard(const char buff[BOARD_ROWS][BOARD_COLS]);
    void writeBoardElement(char elem, int row, int col);

    bool isGameRunning() const { return _gameStats == PLAYER_IN_GAME; };

    static const char SymbolTable[2]; // Player symbols ['X', 'O'].

  private:

    GameServer();
    ~GameServer();

    static GameServer * _instance;

    SocketServer * _socket;
    int _gameStats;

    // The Tic Tac Toe board. This data is sent to all players once changed.
    char _gameBoard[BOARD_ROWS][BOARD_COLS];

    // Mutex to sync the threads.
    Mutex _mtx;

    // Connected clients.
    std::vector<ClientThread *> _clientThreads;
};

// =========================================================
// Client Thread Class
// =========================================================

class ClientThread
{
  public:

    ClientThread();
    ClientThread(Socket * sock, char symbol, int flags);
    ~ClientThread();

    static void threadProc(void * param);

    void setPlayerFlags(int flags);
    void begin();

    int getPlayerFlags() const;
    char getPlayerSymbol() const;
    const Socket * getSocketPtr() const;

  private:

    Socket * _socket;

    // Symbol this player is using. ie: X or O
    char _symbol;

    // Static symbol that stores the last player that made a move.
    static char globalSymbol;

    // Misc flags related to the game.
    int _playerFlags;

    // Signals to the player thread to terminate.
    char _terminate_thread_flag;

    Thread * _t;
};

#endif // T4_GAME_SERVER_HPP
