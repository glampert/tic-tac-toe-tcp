
#include "Common.hpp"
#include "GameServer.hpp"

int main()
{
    std::string Cmd;
    GameServer * gameServer = GameServer::instance();

    try
    {
        if (!gameServer->init("T4.cfg"))
        {
            GameServer::kill();
            return 0;
        }

        do
        {
            gameServer->resetGame();
            std::cout << "Waiting for 2 players to join... \n";
            gameServer->waitForConnections(2);
            gameServer->startGame();

            std::cout << "Type \'reset\' to reset the game server, \'exit\' to close the server.\n";
            std::cin >> Cmd;
        } while (Cmd == "reset");
    }
    catch (...)
    {
        LOG_FATAL_ERROR("Unhandled Exception.");
    }

    GameServer::kill();
    return 0;
}
